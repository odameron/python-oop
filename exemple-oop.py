#! /usr/bin/env python3

class Adresse:
	"""Classe de base pour représenter les adresses sans nom de pays.

	Une adresse est composée de ...
	"""
	typeAdresse = "adresse simple" # attribut de classe : c'est le même pour toutes les insctances

	# 1. constructeur par défaut + créer plusieurs instances (identiques) + les modifier
	#def __init__(self):
	#	self.numRue = 42
	#	self.nomRue = "rue du petit navire"

	# 3. constructueur complet avec valeurs par défaut des params
	def __init__(self, num=42, nom="rue du petit navire"):
		self.numRue = num
		self.nomRue = nom

	# 2. redéfinir str
	def __str__(self):
		return("j'habite au " + str(self.numRue) + " " + self.nomRue + " (" + self.typeAdresse + ")")


if __name__ == "__main__":
	adr1 = Adresse()
	print(adr1)
	adr2 = Adresse(23, "avenue du python")
	print(adr2)

	Adresse.typeAdresse = "nouveau type"
	print(adr1)
	print(adr2)

