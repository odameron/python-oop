Programmation orientée-objets en python

# Contexte

On a vu qu'en python on pouvait être amené-e-s à manipuler des données structurées notamment en combinant des types élémentaires (nombres, booléens,...) et de types composés (chaînes, listes, dictionnaires, tuples,...).
Par exemple, on peut penser à une adresse postale composée d'un numéro, d'une chaîne de caractères pour la rue, d'un nombre pour le code postal, d'une chaîne de caractères pour la ville, etc.

C'est au programmeur de définir comment représenter ces données. 
Par exemple, on aurait pu choisir de représenter une adresse par une liste, dont le premier élement est le numéro de rue, le deuxième élément est le nom de la rue, etc : ``monAdresseListe = [ 3, "rue du petit bateau", 56000, "Lorient"]``.
L'ordre des différents élements est alors purement conventionnel, mais une fois qu'il est établi on doit s'y tenir.
On aurait aussi pu choisir de représenter une adresse par un dictionnaire `monAdresseDict = {"numRue": 3, "codePostal":56000, "nomRue="="rue du petit bateau",...}`, qui rend la structure plus explicite, et qui permet d'accéder à un champ en fonction de son nom (par exemple `monAdresseDict["codePostal"]` au lieu de `monAdresseListe[2]`).
Remarquez que l'on aurait éventuellement pu faire la même chose avec la liste en déclarant les constantes `AdrNumeroRue = 0`, `AdrNomRue = 1`, `AdrCodePostal = 2`... (souvenez vous que par convention en python, les variables dont le nom commence par une majuscule sont considérées comme des constantes) et utiliser ainsi `monAdresseListe["AdrCodePostal"]`, mais on voit bien qu'on commence à bricoler et qu'un cadre devient nécessaire.
Cette façon de procéder est assez intuitive mais a au moins trois limitations principales :

- on aboutit assez vite à des structures complexes : après tout rien n'empèche qu'un des éléments de la liste ou du dictionnaire soit lui-même une liste ou un dictionnaire, et on arrive alors à des structures imbriquées (par exemple, si on voulait dire qu'un nom de rue est lui-même une liste à deux éléments composée d'un type de voie (allée, impasse, rue, avenue,...) et d'un nom de voie. Notre première adresse serait ainsi `[ 3, ["rue", "du petit bateau"], 56000, "Lorient"]`.
- les fonctions qui vont devoir utiliser ces données (et les programmeurs qui vont utiliser ces fonctions) doivent savoir comment les données sont représentées, et s'y conformer, alors que le plus souvent elles souhaitent savoir **comment interagir avec ces données sans avoir à s'inquiéter des répercussions de ces actions sur la façon dont les données sont représentées**. Par exemple j'aimerai pouvoir trouver le code postal d'une adresse sans avoir à m'inquiéter de la position à laquelle ce code postal est rangé dans la liste;
- il est difficile de généraliser pour manipuler des données moins complètes (par exemple des adresses qui ne comporteraient pas de numéro de rue comme pour les lieux-dits), ou au contraire de spécialiser (par exemple en ajoutant une indication de pays).

Les points précédents rendent difficile la réalisation de projets complexes, et encore plus leur maintenance et leur évolution.


# Principe de programmation orientée-objets

## Objets, classes et instances

- on appelle **objet** n'importe quel élément de données, comme par exemple chacune des adresses de la section précédente.
- on appelle **classe** un regroupement d'objets similaires (ici, *similaires* signifie composés des mêmes types d'élements, et aux quels on peut appliquer les mêmes fonctions). On dit alors que chaque objet est une **instance** de la classe. Par exemple les deux adresses de la section précédentes sont des instances de la classe Adresse (par convention, les noms des classes commencent par une majuscule, et les noms des objets par une minuscule). 

Les notions d'*objet* et d'*instance* sont interchangeables.
On parle plutôt d'objet quand on veut désigner un élement de données, et d'instance (d'une classe) quand on veut insister sur la classe à laquelle appartient cet objet.



## Une classe est composée d'attributs et de méthodes

Une classe est ainsi un élement abstrait qui définit :

- la structure commune de ses instances (c'est typiquement ce que l'on faisait à la section précédente lorsque l'on disait qu'une adresse est composée d'un numéro, d'un nom de rue, etc.), 
- et les façons dont on peut interagir avec elles. Par exemple, on peut vouloir accéder aux différents champs d'une adresse, ou les modifier.
    - notion d'**attributs** : les attributs d'une classe sont les champs qu'ont chacune des instances de cette classe (par exemple, `numeroVoie`, `nomRue`, `codePostal`... sont des attributs de la classe `Adresse`; `nom`, `prenom`, `dateNaissance` sont des attributs de la classe `Personne`).
    - notion de **méthodes** : les méthodes d'une classe sont l'ensemble des fonctions que l'on peut appliquer à une instance de cette classe. Par exemple, `donneVoie()` est une méthode de la classe `Adresse` qui renvoie la valeur du numéro et du nom de la rue de l'instance d'adresse à laquelle on l'applique : `adresseRiri.donneVoie()` renvoie le numéro et le nom de la rue de Riri, mais `adresseDonald.donneVoie()` renvoie le numéro et le nom de la rue de Donald.

Remarque 1 : noter le côté abstrait de la classe (qui dit qu'une adresse est composée d'un numéro, d'une chaîne, etc.) et le côté concret des instances, pour laquelle chacun de ces éléments (les attributs) prend une valeur précise.

Remarque 2 : deux instances de la même classe ont la même structure (les mêmes attributs) mais peuvent avoir chacune des valeurs spécifiques pour ces attributs.

Remarque 3 : un objet complexe peut lui-même être composé d'autres objets ou faire référence à d'autres objets via ses attributs. On retrouve une situation similaire lorsqu'on avait souhaité représenter explicitement le type de voie et le nom de la voie : `[ 3, ["rue", "du petit bateau"], 56000, "Lorient"]`. On aurait alors une instance de la classe `Adresse` dont un des attributs prendrait comme valeur une instance de la classe `Voie`, qui aurait lui-même deux attributs `typeVoie` et `nomVoie`. Pour respecter le principe d'encapsulation (cf. ci-dessous), il serait judicieux que la méthode `donneVoie()` de la classe `Adresse` renvoie la concaténation de l'attribut `numeroRue` de la classe adresse et 

- Le **principe d'encapsulation** consiste à n'interagir avec les objets que par l'intermédiaire de leurs méthodes. Les seules méthodes qui accèdent aux attributs d'un objet sont les méthodes de la classe de cet objet (et encore, il peut être judicieux d'avoir quelques méthodes de bas niveau, visibles uniquement depuis l'intérieur de la classe (en les déclarant en `private` en Java) qui gèrent les attributs et qui sont utilisées par les autres méthodes de la classe).
- Le **constructeur** d'une classe est une méthode particulière qui renvoie une nouvelle instance de cette classe. Elle doit s'assurer que tous les attributs de cette instance ont reçu une valeur.

**FIXME:** faut-il par principe déclarer les attributs en private en les entourant d'underscore ?


## Exemple d'utilisation modulaire de classes et d'objets

- classe Adresse
- classe Personne : nom, prénom, numSecSociale, adresse, amis
- Société : nom, adresse, salariés (chaque salarié est une personne qui a elle même une adresse)


## Généraliser et spécialiser : super-classes, sous-classes et héritage

On peut vouloir qu'une classe (appelée **sous-classe**) hérite d'une classe plus générale (appelée **super-classe**).

Dans ce cas :
- la sous-classe hérite de tous les attributs et de toutes les méthodes de la super-classe
- la sous-classe peut ajouter de nouveaux attributs et/ou de nouvelles méthodes
- la sous-classe peut surcharger (notion d'override en Java) les méthodes de la superclasse (**FIXME:** notion de super ?)
- on peut utiliser une instance de la sous-classe à tous les endroits où on utiliserait une instance de la superclasse (par exemple, donner une instance de `AdresseInternationale` comme valeur de l'attribut adresse d'une personne, qui attendait une instance de `Adresse`
 

# Programmation orientée-objets en python

## Déclaration d'une classe

- La déclaration d'une classe se fait avec le mot-clé `class`, suivi du nom de la classe (qui commence par une majuscule, par convention), puis de ":" qui indique que le corps de la classe commence à la ligne suivante
- c'est une bonne idée de mettre une docstring qui documente votre classe au début de son corps, juste après la déclaration
- Tout le corps de la classe est indenté de 1 (4 espaces ou une tabulation) par rapport à la déclaration
- Une fois que l'on a quitté le corps de la classe, on peut définir des instructions de manière classique
    - évitez et mettez-les plutôt dans un script séparé si vous envisagez de partager et réutiliser votre classe
    - au minimum, mettez au moins le code qui ne fait pas partie de la classe dans un test `if __name__ == "__main__":` pour qu'il ne soit exécuté que si vous utilisez votre script directement.

```python
class Adresse:
	"""Une classe vide pour le moment."""
	pass

# ici on a quitté le corps de la classe
if __name__ == "__main__":
	# cette partie du code n'est exécutée que si on appelle
	# directement le script
	#
	# création d'une instance de la classe Adresse
	monAdresse = Adresse()

	# attributs et méthodes appliquables sur cette instance
	dir(monAdresse)
	# renvoie:
	# ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']

	# remarquez que chaque objet sait de quelle classe il est une instance via son attribut __class__
	monAdresse.__class__
	# renvoie <class '__main__.Adresse'>

	# chaque objet hérite aussi de la docstring de sa classe
	monAdresse.__doc__
	# renvoie 'Une classe vide pour le moment.'
	# et 
	monAdresse.__class__.__doc__
	# renvoie également 'Une classe vide pour le moment.'
```


## Constructeur et déclaration des attributs

- Déclaration (dans le corps de la classe) : le constructeur d'une classe est la méthode `def __init__(self, ...):`
- Son premier argument doit être `self` qui désigne l'objet que le constructeur est en train de créer
- Le rôle du constructeur est d'affecter une valeur raisonnable à tous les attributs de la nouvelle instance
- Le corps du constructeur est indenté d'un niveau par rapport à sa déclaration (donc de deux niveaux par rapport à la déclaration de la classe).
- Python ne permet pas de déclarer plusieurs constructeurs, mais on peut donner des valeurs par défaut aux arguments
- En Python, les attributs d'une classe sont définis dans le constructeur


```python
class Adresse:
	"""Une version simplifiée d'une adresse."""
	
	# seuls les attributs de classe (equivalent à static en Java) 
	# sont déclarés ici
	# ils sont partagés par toutes les instances
	typeAdresse = "adresse simple" 
	# les attributs d'instance (non static)
	# ne sont pas déclarés ici mais dans le constructeur

	def __init__(self, num=42, nom="rue du petit navire", codePostal=56000, ville="Lorient"):
		self.numRue = num
		self.nomRue = nom
		self.zipcode = codePostal
		self.nomVille = ville

# ici on a quitté le corps de la classe
if __name__ == "__main__":
	# cette partie du code n'est exécutée que si on appelle
	# directement le script
	#
	# création d'une instance de la classe Adresse
	# remarquez que l'on ne renseigne pas l'argument self...
	# ... ce qui est normal puisque le constructeur est justement
	# responsable de créer l'objet et de peupler ses attributs
	monAdresse = Adresse()

	# remarquez que les attributs sont maintenant bien attachés à l'instance
	dir(monAdresse)
	# renvoie ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', 'nomRue', 'nomVille', 'numRue', 'typeAdresse', 'zipcode']
	# y compris les attributs de classe
	print(monAdresse.typeAdresse)

	# on crée une deuxième instance
	monAutreAdresse = Adresse(12, "bd Baille", 13006, "Marseille")

	# chaque instance a ses propres valeurs d'attributs
	print(monAdresse.nomRue)		# renvoie "rue du petit navire"
	print(monAutreAdresse.nomRue)	# renvoie "bd Baille"
	# sauf l'attribut de classe, qui est partagé
	print(monAdresse.typeAdresse)		# renvoie "adresse simple"
	print(monAutreAdresse.typeAdresse)	# renvoie "adresse simple"
	Adresse.typeAdresse = "nouvelle valeur de l'attribut de classe"
	print(monAdresse.typeAdresse)		# renvoie "anouvelle valeur de l'attribut de classe"
	print(monAutreAdresse.typeAdresse)	# renvoie "anouvelle valeur de l'attribut de classe"
```


## Méthodes (d'instances)

- Déclaration (dans le corps de la classe) : `def nomMethode(self, arg1, ..., argn):`
- Il faut faire référence explicitement à `self` pour accéder aux attributs, sinon la référence est considérée comme une variable locale
- Appel : instance.nomMethode(arg1, ..., argn)
- remarquer que nomInstance sert à donne la valeur de self et que les autres arguments sont utilisés de façon habituelle

```python
class Adresse:
	"""Une version simplifiée d'une adresse."""
	
	# seuls les attributs de classe (equivalent à static en Java) 
	# sont déclarés ici
	typeAdresse = "adresse simple" 

	def __init__(self, num=42, nom="rue du petit navire", codePostal=56000, ville="Lorient"):
		self.numRue = num
		self.nomRue = nom
		self.zipcode = codePostal
		self.nomVille = ville

	def donneRueComplete(self):
		return self.numRue + " " + self.nomRue

if __name__ == "__main__":
	monAdresse = Adresse()
	monAutreAdresse = Adresse(12, "bd Baille", 13006, "Marseille")

	print(monAdresse.donneRueComplete())		# renvoie "3 rue du petit navire"
	print(monAutreAdresse.donneRueComplete())	# renvoie "12 bd Baille"
```



## À TRIER


- parler de `self`

- déclaration de class
    - indentation
    - déclaration des attributs : dans le constructeur
    - déclaration des méthodes : dans la partie indentée de la classe
        - méthode d'instance : 
            - arguemt self est le 1er argument
            - on n'a pas besoin de le renseigner lorsqu'on appelle la méthode
        - méthode de classe (aka méthode statique) : pas d'argument self
- constructeur
    - déclaration des attributs se fait dans le constructeur
    - on peut aussi en déclarer ailleurs, voir directement sur une instance une fois qu'elle est créée, mais ce n'est sans doute pas une bonne idée
    - possible d'utiliser les valeurs par défaut des arguments
- méthodes
    - appel : instance.nomMethode(arg1, ..., argn)
    - remarquer que nomInstance sert à donne la valeur de self et que les autres arguments sont utilisés de façon habituelle

- fair des dir() pour montrer les attributs et les méthodes
    
- print_address(self)
    - l'argument self indique méthode d'instance, par opposition aux méthodes statiques

- en dehors du bloc : instanceAddr = Adress(12, "bd Baille", 13006, "Marseille")


## Déclaration minimale
```python
class Adresse:
    """Short description of the class in a docstring."""
    pass

# ici on est sortis
adresseMysterieuse = Adress()

# 
dir(adresseMysterieuse)
# renvoie:
# ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__']
```




```python
class Adresse

    donneVoie(self):
		# ou mieux : self.donneNumeroRue() + self.voie.donneNomVoie()
        return self.numeroRue + self.voie.donneNomVoie()
	

```

